class MetodosOrdenamiento extends DatosMetodo{

	public void ordenarBurbujaUno(int numeros[]) {
		this.clean();

		long tInicio = System.nanoTime();

		for (int i = 0; i <= numeros.length; i++) {
			this.setCantidadPasadas(getCantidadPasadas()+1);

			for (int j = 0; j < numeros.length-1; j++) {
				this.setCantidadComparaciones(getCantidadComparaciones()+1);
				if (numeros[j]> numeros[j+1]) {
					if (true) {
						int aux = numeros[j];
						this.setCantidadIntercambios(getCantidadIntercambios()+1);
						numeros[j]=numeros[j+1];
						numeros[j+1]=aux;

					}

				}
			}
		}

		long tFin = System.nanoTime();
		this.impresion(numeros);
		this.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		this.clean();

	}//Burbuja 1

	public void ordenarBurbujaDos(int numeros[]) {
		this.clean();

		long tInicio = System.nanoTime();

		int i = 1;
		boolean ordenado = false;
		while ((i <= numeros.length) || (ordenado == false)) {

			this.setCantidadPasadas(getCantidadPasadas()+1);

			i = i + 1;
			ordenado = true;
			for (int j = 0; j <= (numeros.length - i); j++) {

				this.setCantidadComparaciones(getCantidadComparaciones()+1);

				if (numeros[j] > numeros[j + 1]) {

					this.setCantidadIntercambios(getCantidadIntercambios()+1);

					ordenado = false;
					int aux = numeros[j];
					numeros[j] = numeros[j + 1];
					numeros[j + 1] = aux;

				}
			}

		}

		long tFin = System.nanoTime();
		this.impresion(numeros);
		this.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		this.clean();
	}// Burbuja 2

	public void ordenarBurbujaTres(int numeros[]) {
		this.clean();

		long tInicio = System.nanoTime();

		int i = 1;
		boolean ordenado = true;

		do {

			this.setCantidadPasadas(getCantidadPasadas()+1);

			i = i + 1;
			for (int j = 0; j <= (numeros.length - i); j++) {

				this.setCantidadComparaciones(getCantidadComparaciones()+1);

				if (numeros[j] > numeros[j + 1]) {
					ordenado = false;
					int aux = numeros[j];
					numeros[j] = numeros[j + 1];
					numeros[j + 1] = aux;
					this.setCantidadIntercambios(getCantidadIntercambios()+1);
				}
			}

		} while (i < numeros.length || ordenado == true);

		long tFin = System.nanoTime();
		this.impresion(numeros);
		this.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		this.clean();
	}// Burbuja 3

	// ================================== Fin Metodo Burbuja ===================================

	public void quickSort(int numeros[], int menor, int mayor) {

		this.setCantidadPasadas(getCantidadPasadas()+1);

		if (menor < mayor) {

			int pi = particion(numeros, menor, mayor);

			quickSort(numeros, menor, pi - 1);
			quickSort(numeros, pi + 1, mayor);

		}
	}

	public int particion(int numeros[], int menor, int mayor) {
		int pivot = numeros[mayor];
		int i = (menor - 1);

		for (int j = menor; j < mayor; j++) {

			this.setCantidadComparaciones(getCantidadComparaciones()+1);

			if (numeros[j] < pivot) {
				i++;

				this.setCantidadIntercambios(getCantidadIntercambios()+1);

				int temp = numeros[i];
				numeros[i] = numeros[j];
				numeros[j] = temp;

			}
		}

		int temp = numeros[i + 1];
		numeros[i + 1] = numeros[mayor];
		numeros[mayor] = temp;

		return i + 1;
	}

	// ================================== Fin Metodo Quicksort ===================================

	public void shellSort(int numeros[]) {
		this.clean();

		long tInicio = System.nanoTime();

		int n = numeros.length; 
		for (int gap = n/2; gap > 0; gap /= 2){ 

			this.setCantidadPasadas(getCantidadPasadas()+1);

			for (int i = gap; i < n; i += 1){ 

				this.setCantidadComparaciones(getCantidadComparaciones()+1);

				int temp = numeros[i];
				int j; 
				for (j = i; j >= gap && numeros[j - gap] > temp; j -= gap) 
					numeros[j] = numeros[j - gap];
				numeros[j] = temp; 
				this.setCantidadIntercambios(getCantidadIntercambios()+1);
			} 
		} 

		long tFin = System.nanoTime();
		this.impresion(numeros);
		this.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		this.clean();

	}

	// ================================== Fin Metodo Shellsort ===================================

	public void radixSort (int numeros []) {
		this.clean();

		long tInicio = System.nanoTime();

		int x, i, j;
		for(x=Integer.SIZE-1; x>=0; x--){
			
			this.setCantidadPasadas(getCantidadPasadas()+1);
			
			int auxiliar[]= new int[numeros.length]; 
			j=0;
			for(i=0;i<numeros.length; i++ ){  
				
				this.setCantidadComparaciones(getCantidadComparaciones()+1);
				
				boolean mover= numeros[i]<<x>=0; // true o un false
				
				if(x==0 ? !mover: mover){ //evaluar y comparar 
					auxiliar[j]= numeros[i]; //correcto
					j++;
					
					this.setCantidadIntercambios(getCantidadIntercambios()+1);
					
				}else{ //de lo contrario
					numeros[i-j]= numeros[i]; 
					
					this.setCantidadIntercambios(getCantidadIntercambios()+1);
					
				}
			}
			for(i=j; i<auxiliar.length; i++){ //copear lo del arreglo auxiliar
				auxiliar[i]= numeros[i-j];
				
				this.setCantidadIntercambios(getCantidadIntercambios()+1);
				
			}
			numeros = auxiliar;
			
		}

		long tFin = System.nanoTime();
		this.impresion(numeros);
		this.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		tInicio = tFin =0;
		this.clean();
	}

	// ================================== Fin Metodo Radixsort ===================================

	public void intercalacion(int[] arregloA, int[] arregloB) {
		this.clean();
		
		long tInicio = System.nanoTime();

		int i,j,k;
		int arregloC[] = new int [arregloA.length + arregloB.length]; 

		//Repetir minetras los arreglos A y B tengan elementos a comparar 
		for (i = j = k = 0; i < arregloA.length &&  j < arregloB.length ; k++) {

			this.setCantidadPasadas(getCantidadPasadas()+1);
			this.setCantidadComparaciones(getCantidadComparaciones()+1);

			if (arregloA[i]< arregloB[j]) {
				arregloC[k]= arregloA[i];
				i++;

				this.setCantidadIntercambios(getCantidadIntercambios()+1);

			}else {
				arregloC[k]=arregloB[j];
				j++;

				this.setCantidadIntercambios(getCantidadIntercambios()+1);

			}

		}

		// A�adir sobrantes del arreglo A en caso de haberlos 
		for(; i < arregloA.length; i++, k++ ) {
			arregloC[k]= arregloA[i];

			this.setCantidadIntercambios(getCantidadIntercambios()+1);

		}

		// A�adir sobrantes del arreglo B en caso de haberlos 
		for(; j < arregloB.length; j++, k++ ) {
			arregloC[k]= arregloA[j];

			this.setCantidadIntercambios(getCantidadIntercambios()+1);

		}

		long tFin = System.nanoTime();
		this.impresion(arregloC);
		this.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		this.clean();

	}

	// ================================== Fin Metodo Intercalacion ===================================
	
	public int[] mezclaDirecta(int numeros[]) {
			
		long tInicio = System.nanoTime();
		
			int i, j, k;
			
			if(numeros.length > 1) {
				
				int nElementosIzq = numeros.length/2;
				int nElementosDer = numeros.length - nElementosIzq;
				int arregloIzq[] = new int[nElementosIzq];
				int arregloDer[] = new int[nElementosDer];
				
				//Cepiando los elementos al primer arreglo
				for(i = 0; i < nElementosIzq; i++) {
					arregloIzq[i] = numeros[i];
					this.setCantidadIntercambios(getCantidadIntercambios()+1);
				}
				//Copiando los elementos al segundo arreglo
				for (i = nElementosIzq; i < nElementosIzq + nElementosDer; i++) {
					arregloDer[i-nElementosIzq] = numeros[i];
					this.setCantidadIntercambios(getCantidadIntercambios()+1);
				}
				//Recursividad
				arregloIzq = mezclaDirecta(arregloIzq);
				arregloDer = mezclaDirecta(arregloDer);
				this.setCantidadPasadas(getCantidadPasadas()+2);
				
				i = 0;
				j = 0;
				k = 0;
				
				while(arregloIzq.length != j && arregloDer.length != k) {
					if(arregloIzq[j] < arregloDer[k]) {
						numeros[i] = arregloIzq[j];
						i++;
						j++;
						this.setCantidadIntercambios(getCantidadIntercambios()+1);
					}else {
						numeros[i] = arregloDer[k];
						i++;
						k++;
						this.setCantidadIntercambios(getCantidadIntercambios()+1);
					}
					this.setCantidadComparaciones(getCantidadComparaciones()+1);
				}
				//Arreglo final
				while(arregloIzq.length != j) {
					numeros[i] = arregloIzq[j];
					i++;
					j++;
					this.setCantidadIntercambios(getCantidadIntercambios()+1);
				}
				while(arregloDer.length != k) {
					numeros[i] = arregloDer[k];
					i++;
					k++;
					this.setCantidadIntercambios(getCantidadIntercambios()+1);
				}
			}//fin del if
			
			return numeros;
			
		}
	
	// ================================== Fin Metodo Mezcla directa ===================================
	
	public void mezclaNatural(int[] numeros) {
		
		long tInicio = System.nanoTime();
		
		
		int izquierda = 0;
		int izq = 0;
		int derecha = numeros.length-1;
		int der = derecha;
		boolean ordenado = false;
		do {
			ordenado = true;
			izquierda = 0;
			while (izquierda < derecha) {
				this.setCantidadComparaciones(getCantidadComparaciones()+1);
				izq = izquierda;
				while(izq < derecha && numeros[izq] <= numeros[izq+1]) {
					izq++;
					this.setCantidadComparaciones(getCantidadComparaciones()+1);
				}
				der = izq+1;
				while(der == derecha-1 || der < derecha && numeros[der] <= numeros[der+1]) {
					der++;
					this.setCantidadComparaciones(getCantidadComparaciones()+1);
				}
				if (der <= derecha) {
					this.setCantidadComparaciones(getCantidadComparaciones()+1);
					mezclaDirecta2(numeros);
					ordenado = false;
				}
				izquierda = izq;
			}
		} while (!ordenado);
		
		
		long tFin = System.nanoTime();
		this.impresion(numeros);
		this.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		tInicio = tFin =0;
		this.clean();
		
	}
	
	
	public void mezclaDirecta2(int[] arreglo) {
		int i,j,k;
		if (arreglo.length>1) {
			int nElementosIzq=arreglo.length/2;
			int nElementosDer=arreglo.length-nElementosIzq;
			int arregloIzq[]=new int [nElementosIzq];
			int arregloDer[]=new int [nElementosDer];
			
			for (i = 0; i < nElementosIzq; i++) {
				arregloIzq[i]=arreglo[i];
			
				this.setCantidadPasadas(getCantidadPasadas()+1);
			}
			for (i = nElementosIzq; i < nElementosIzq+nElementosDer; i++) {
				arregloDer[i-nElementosIzq]=arreglo[i];
				
				this.setCantidadPasadas(getCantidadPasadas()+1);
			}
			arregloIzq=mezclaDirecta(arregloIzq);
			arregloDer=mezclaDirecta(arregloDer);
			i=0;
			j=0;
			k=0;
			while(arregloIzq.length!=j&&arregloDer.length!=k) {
				
				this.setCantidadPasadas(getCantidadPasadas()+1);
				this.setCantidadComparaciones(getCantidadComparaciones()+1);
				if(arregloIzq[j]<arregloDer[k]) {
					arreglo[i]=arregloIzq[j];
					i++;
					j++;
					
					this.setCantidadIntercambios(getCantidadIntercambios()+1);
					
				}else {
					arreglo[i]=arregloDer[k];
					i++;
					k++;
					
					this.setCantidadIntercambios(getCantidadIntercambios()+1);
				}
			}
			while(arregloIzq.length!=j) {
				arreglo[i]=arregloIzq[j];
				i++;
				j++;
				this.setCantidadPasadas(getCantidadPasadas()+1);
			}
			while(arregloDer.length!=k) {
				arreglo[i]=arregloDer[k];
				i++;
				k++;
				this.setCantidadPasadas(getCantidadPasadas()+1);
			}
		}//fin del if
	}
	
	// ================================== Fin Metodo Mezcla natural ===================================
	

		
	    int busquedaBinaria(int vector[], int posicionIzq, int posicionDer, int elementoA_Buscar){ 
	    	this.clean();
	    	long tInicio = System.nanoTime();
	    	
	    	this.setCantidadComparaciones(getCantidadComparaciones()+1);
	    	
	        if (posicionDer >= posicionIzq) { 
	            int mitad = posicionIzq + (posicionDer - posicionIzq) / 2; 
	            this.setCantidadComparaciones(getCantidadComparaciones()+1);
	            
	            if (vector[mitad] == elementoA_Buscar) {
	            	this.setCantidadComparaciones(getCantidadComparaciones()+1);
	                return mitad; 
	            }
	            
	            if (vector[mitad] > elementoA_Buscar) {
	            	this.setCantidadComparaciones(getCantidadComparaciones()+1);
	                return busquedaBinaria(vector, posicionIzq, mitad - 1, elementoA_Buscar); 
	                
	            }
	            this.setCantidadPasadas(getCantidadPasadas()+1);
	            return busquedaBinaria(vector, mitad + 1, posicionDer, elementoA_Buscar); 
	        } 
	        
	        
			long tFin = System.nanoTime();
			this.informacionEjecucion();
			System.out.println("Tiempo: "+(tFin-tInicio));
			tInicio = tFin =0;
			this.clean();
	        
	        return -1;
	} 
	
	
	// ================================== Fin Metodo Busqueda Secuencial y Binaria ===================================
	
}//Clase MetodosOrdenamiento

public class Meto_Orde {

	public static void main(String[] args) {

	}

}
